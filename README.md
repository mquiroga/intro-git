# Setting up folder structure / project layout

Some useful links on good practices for setting up your project/folders:

[Best practices for scientific computing](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001745)

[Good enough practices in scientific computing](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005510)

[In R](http://swcarpentry.github.io/r-novice-gapminder/02-project-intro/index.html)

Following the software carpentry link, start by creating the following subfolders in your project folder:

* `src` is where you will save the scripts that you work on (Stata, R, python, XML, JSON, etc)
* `bin` is for storing any executables that are needed for your project
* `results` is where you will save the output from your code/scripts
* `doc` is used for storing documents related to the project
* `data` is for storing your raw and processed data

You can add subdirectories within these to organise your files. Eg you might decide you want to have `data/raw` and `data/processesd` under `data`.

## Options for setting up project folder and getting it into git for version control

You have a few different options for setting up your project folder and putting it into version control. You can pick one and follow the instructions (or if there are no instructions yet, please feel free to add some!)

1. Github desktop (free but only supports some basic git features) <- please feel free to add further instructions
2. [Git Kraken](clients/gitkraken.md) (Pro account needed to use it with self-managed GitLab - pro account available for free for GitHub education accounts, available for free if you have a .edu email address)
3. Sublime Merge (Paid but with an unlimited free trial period) <- please feel free to add further instructions
4. Git bash (Windows) or git command line (Mac, Linux) (Efficient but esoteric) <- please feel free to add further instructions
5. VS Code <- please feel free to add further instructions

## Setting up for collaboration

If you are collaborating with others on a project then you need to make sure that your collaborators can access the remote repository on gitlab.unimelb.edu.au

1. In a web browser login to [https://gitlab.unimelb.edu.au/](https://gitlab.unimelb.edu.au/)
2. Click on your project
3. Click on "Settings" then "Members"
4. Add email addresses for anyone you wish to invite and assign a role and expiry date (if timebound)

## Saving time and effort: creating a template

You can create a template folder on your local computer that is set up with your preferred subfolder structure and a .gitignore file that already contains entries that you will use in all of your projects

Whenever you start a new project you can use that template. You can use the above workflow and then copy in the template to your folder after cloning.


## Adding a git ignore file

A gitignore file tells git what things to exclude from version control tracking. You usually create a gitignore file (named `.gitignore`) in the top level of your project folder. `.gitignore` is a plain text file. You can add new lines to the `.gitignore` file to tell git what to exclude. It's also possible to add git ignore files in subdirectories if you need more control but for now let's focus on a git ignore in the top level.

**What should we ignore?**

With our project folder there are several folders we don't want to put into version control:

* `data` because git repositories aren't a great place to store data and data formats that are not plain text don't play well with git (see [dvc.org](https://dvc.org/) instead)
* `bin` because these should be compiled binaries that we can't edit anyway and are hard for git to track
* `results` because we should treat the output of our analysis as disposable
* `doc` because if it's anything like my doc folders it's a) all reference material and b) it's chock full of files and c) we shouldn't have anything in our code that depends on the contents of the doc folder

Let's add those to our gitignore.

1. Open your repository's .gitignore file using any text editor (notepad, VS Code, Sublime, etc.)
2. In a new line, type in `/data`
3. In a new line, type in `/bin`
4. In a new line, type in `/results`
5. In a new line, type in `/doc`
6. Save the file

Now the contents of those four folders will not be tracked.

You can also use wild cards to ignore specific file types eg adding an entry `**/*.pdf` will tell git not to track any files ending in `.pdf` anywhere in your repository.

You can learn more about the syntax for entries in `.gitignore` files on this page about [git ignore patterns](https://git-scm.com/docs/gitignore).

Github also has a [repository of templates](https://github.com/github/gitignore) for `.gitignore` files for different programming languages and projects.

## Using git in your IDE

### Stata

[Stata](https://github.com/mpaulacaldas/git-with-stata) users can issue commands to git by setting the working directory to the desired git repo and tpying in `shell` followed by the git command in interactive mode.

### rStudio

If you save a project file in your folder, you will get a built-in git client into RStudio (see, for example [Using git from RStudio](https://nceas.github.io/oss-lessons/version-control/4-getting-started-with-git-in-RStudio.html)). Once you have a project file, and a git repository initialised on the same project, and extra "Git" tab will appear in the same pane as "Environment", "Files" and "Plots"; from where you can interact with your repository.

You can also use the terminal tab (usually grouped with the "Console") and type there the same commands you would type in git bash.

### Sublime text.

1. Via commnand line. Install the `terminus` package. `Command + shift + P` then `term view`. Then enter your git commands in the terminal view
2. Via command palette. Install the `git` package. When you need to perform a git command press `Command + shift + p` and type `git` into the command palette and then choose the command you want to run.

Please add more!
