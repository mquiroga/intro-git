## Obtaining GitKraken Pro for free

1. If you do not have a GitHub account, [sign up for free](https://github.com/) using a .edu email address. If you already have a GitHub account, make sure you have added a .edu email address, and set it as your primary email.
2. If you have not upgraded your free GitHub account to a Pro account (for free through the educational license), do so [here](https://education.github.com/benefits). Under "Individuals", get student benefits if you are currently a student, and teacher benefits if you are employed as academic staff. This could take some time to be approved, so be patient!

![GitHub education](../img/github-education.png)

3. Download [GitKraken](https://www.gitkraken.com/download).
4. Open GitKraken and sign in with your pro GitHub account to get the pro version of GitKraken for free (normally $49/user/year). You will see an orange "pro" on the bottom right:

![GitKraken pro](../img/gitkraken-pro.png)

## Connecting your GitKraken Pro account with your Unimelb GitLab hosting server

1. In GitKraken, go to File -> Preferences -> Authentication -> GitLab Self-Managed
2. Follow the prompts to create a token on the Unimelb GitLab browser interface (enter gitlab.unimelb.edu.au into the Host Domain field to prompt "Generate a token on GitLab to appear")
![Connect GitKraken to self-managed GitLab](../img/gitkraken-login.png)

3. Once you go back to GitKraken and see the green checkmark + Connected, you're good to go!

![Connect GitKraken to self-managed GitLab](../img/gitkraken-authenticate.png)

## Create a new repository

1. In GitKraken, go to File -> Init Repo
2. Choose where you would like to host this new repository. If on the Unimelb GitLab, choose "GitLab (Self-managed)"
3. Follow the prompts!
4. (Optional) Use one of the suggested .gitignore templates, or create one later yourself
5. (Optional) Add a license to your repository, choose from one of the suggested options

![Create new repository in GitKraken](../img/gitkraken-init.png)

## Clone an existing repository

1. In GitKraken, go to File -> Clone Repo
2. Because you've connected GitKraken to your hosting services, it will automatically know every repository that you have access to, and will display them in a drop-down menu.

![Clone repository in GitKraken](../img/gitkraken-clone.png)

## Stage and commit

In git the workflow is to edit your files, then (stage) identify which edits you want to put into version control, and then to `commit` the staged edits.

In GitKraken the changed files are shown in the right-hand pane. You have different options for how much you want to `stage`:

![Staging in GitKraken](../img/Inkedgitkraken-stage.jpg)

1. You can `stage` all changes by clicking `Stage all changes`
2. You can `stage` a whole file by clicking on the file and then `Stage File` at the top of the pane
3. You can `stage` specific parts within a file (`hunks`) by clicking `Stage Hunk`
4. You can change your mind about a part of the file you didn't mean to change by clicking `Discard Hunk`

Once you've staged all the content that you want to commit:

5. Add a title for the changes you have made in this commit (use keywords that you might want to use later for searching for this particular commit)
6. Commit the changes by clicking the `Commit changes` button at the bottom right (the message will change after staging files/hunks)

This will stage and commit the changes in your local git repository

## Push your changes

Click the `Push` up arrow at the top of GitKraken to push your changes to the remote on the Unimelb git server

Keep in mind that if you have empty folders in your project they will not appear on the remote repository. They will be added once they contain files and those files have been committed and pushed
